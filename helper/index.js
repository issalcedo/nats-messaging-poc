const NATS = require('nats');
const nats = NATS.connect('nats://natsio:4222');

nats.on('connect', () => {
    nats.subscribe('helper', (msg) => {
        //console.log(`MSG ${msg}`);
        const payload = JSON.parse(msg);

        setTimeout(() => {
            const replyTo = payload.txId;
            //console.log(`SUBJECT ${replyTo}`);
            nats.publish(replyTo, 'reply');
        }, 500);
    });
});