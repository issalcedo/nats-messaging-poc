const NATS = require('nats');
const nats = NATS.connect('nats://natsio:4222');

function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

nats.on('connect', () => {
    nats.subscribe('helper', (msg) => {
        const payload = JSON.parse(msg);
        const replyTo = payload.txId;
        const loop = payload.loop;

        let sid = null;
        let received = 0;
        const expected = loop;
        const txId = makeid();

        payload.txId = txId;
        payload.loop = 1;

        sid = nats.subscribe(txId, function () {
            received += 1;
            if (received === expected) {
                nats.publish(replyTo, 'reply');
            }
        });

        // Timeout unless a certain number of messages have been received
        nats.timeout(sid, 3500, expected, function () {
            if (!sent) {
                console.log('not sure what happened');
            }
        });

        for (let i = 0; i < loop; i++) {
            nats.publish('looper', JSON.stringify(payload));
        }
    });
});