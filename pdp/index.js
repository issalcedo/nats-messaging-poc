const NATS = require('nats');
const nats = NATS.connect('nats://natsio:4222');
const express = require('express');
const bodyParser = require('body-parser');

function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

nats.on('connect', () => {

    const app = express();
    app.use(bodyParser.json());

    app.get('/decision', (req, res) => {
        const start = Date.now();
        const loop = parseInt(req.query.loop, 10); //10
        const helperLoop = parseInt(req.query.helperLoop, 10);
        const wait = parseInt(req.query.wait, 10);
        const label = req.query.label;

        const payload = {hello: 'world'};
        const txId = makeid();
        payload.txId = txId;
        payload.loop = helperLoop;
        payload.wait = wait;
        let sent = false;
        let sid = null;
        const reply = (code, msg) => {
            if (!sent) {
                //console.log(code, msg);
                const stop = Date.now();
                console.log(`${label},${stop - start}`);
                res.status(code).send(msg);
                sent = true;
                if (sid !== null) {
                    nats.unsubscribe(sid);
                }
            }
        }

        let received = 0;
        const expected = loop;

        sid = nats.subscribe(txId, function () {
            received += 1;
            //console.log('recieved', received);
            if (received === expected) {
                reply(200, ':)');
            }
        });

        // Timeout unless a certain number of messages have been received
        nats.timeout(sid, 3500, expected, function () {
            if (!sent) {
                reply(503, ':(');
            }
        });

        //console.log(expected, loop);
        for (let i = 0; i < loop; i++) {
            //console.log('looping', i);
            nats.publish('helper', JSON.stringify(payload));
        }
    });

    app.listen(3000, ()=>{
        console.log('app started');
    })
});