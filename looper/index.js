const NATS = require('nats');
const nats = NATS.connect('nats://natsio:4222');

function evaluatePolicy(i) {
    if (i === 1) {
        return 1;
    }

    return i * evaluatePolicy(i - 1);
}

function couchbaseFetchById(wait, callback) {
    setTimeout(() => {
        callback();
    }, wait); //get policy
}

nats.on('connect', () => {
    nats.subscribe('looper', (msg) => {
        const payload = JSON.parse(msg);
        const loop = payload.loop;
        const wait = payload.wait;

        for (let i = 0; i < loop; i++) {
            couchbaseFetchById(wait, ()=>{
                const replyTo = payload.txId;
                evaluatePolicy(100);
                nats.publish(replyTo, 'reply');
            });
        }
    });
});