///////////////////////////////////////
// Request Performance
///////////////////////////////////////
const request = require('request');
const fs = require('fs');
const loop = 5000;
const hash = 1000;
let received = 0;

console.log('Request Performance Test');

var start = new Date();

// Need to flush here since using separate connections.

function benchMarkPDPLooping() {
    for (var i = 0; i < loop; i++) {
        request({
            'url': 'http://localhost:3000/decision?loop=10&helperLoop=1',
            'method': 'POST',
            'body': { "hello": "world" },
            'json': true
        }, function (e, r, body) {
            received += 1;
            if (received === loop) {
                var stop = new Date();
                var rps = parseInt(loop / ((stop - start) / 1000), 10);
                console.log('\n' + rps + ' request-responses/sec');
                var lat = parseInt(((stop - start) * 1000) / (loop * 2), 10); // Request=2, Reponse=2 RTs
                console.log('Avg roundtrip latency: ' + lat + ' microseconds');
                log("rr", loop, stop - start);
            } else if (received % hash === 0) {
                process.stdout.write('+');
            }
        });
    }

}

function log(op, count, time) {
    fs.appendFile('rr.csv', [op, count, time, new Date().toDateString(), "one request"].join(",") + "\n", function (err) {
        if (err) {
            console.log(err);
        }
        process.exit();
    });
}